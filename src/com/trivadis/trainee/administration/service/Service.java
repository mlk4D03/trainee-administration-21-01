package com.trivadis.trainee.administration.service;

import com.trivadis.trainee.administration.model.Trainee;
import com.trivadis.trainee.administration.sql.DefaultChanges;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class Service {
    // JDBC driver name and database URL and Database credentials
    static final String DB_URL = "jdbc:mysql://localhost/trainee_administration";
    static final String USER = DefaultChanges.getUSER();
    static final String PASS = DefaultChanges.getPW();

    //Opens a connection
    public static Statement getService() {
        Connection conn;
        Statement stmt = null;
        try {
            // Schritt 1: Laden des JDBC Treibers
            Class.forName(DefaultChanges.getMySqlDriver());

            // Schritt 2: Aufbau einer Verbindung
            conn = DriverManager.getConnection(DB_URL, USER, PASS);

            // Schritt 3: Erzeugen eines Statement-Objekts
            stmt = conn.createStatement();
        } catch (SQLException se) {
            se.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return stmt;
    }
}


