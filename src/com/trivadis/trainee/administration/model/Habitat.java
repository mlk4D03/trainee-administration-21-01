package com.trivadis.trainee.administration.model;

public enum Habitat {
       BASEL("Basel"),
       BERN("Bern"),
       BRUGG("Brugg"),
       BUKAREST("Bukarest"),
       DUESSELDORF("Düsseldorf"),
       FRANKFURT_AM_MAIN("Frankfurt am Main"),
       FREIBURG("Freiburg"),
       GENF("Genf"),
       HAMBURG("Hamburg"),
       LAUSANNE("Lausanne"),
       MANNHEIM("Mannheim"),
       MUENCHEN("München"),
       STUTTGART("Stuttgart"),
       WIEN("Wien"),
       ZUERICH("Zürich");

       private String city;

       Habitat(String habitat) {
              this.city = habitat;
       }

       public String getHabitat() {
              return city;
       }
    }


