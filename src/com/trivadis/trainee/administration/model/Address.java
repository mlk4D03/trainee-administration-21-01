package com.trivadis.trainee.administration.model;

public class Address {

    private String street;
    private String zip;
    private String number;
    private Habitat habitat;

    public Address(String street, String zip, String number, Habitat habitat){
        setStreet(street);
        setZip(zip);
        setNumber(number);
        setHabitat(habitat);
    }

    public Habitat getHabitat() {
        return habitat;
    }

    public void setHabitat(Habitat habitat) {
        this.habitat = habitat;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getStreet() {
        return street;
    }

    public String getZip() {
        return zip;
    }

    public String getNumber() {
        return number;
    }

    public String getAddress(Habitat location) {
        return location + " " + this.getZip() +  " " + this.getStreet() + " " + this.getNumber();
    }
}
