package com.trivadis.trainee.administration.model;

public class Trainee extends Person{

    private Course.CourseName courseName;
    private ageGroup ageGroup;
    private int previousKnowledge;

    public enum ageGroup {
        YEAR181("18_1"),
        YEAR182("18_2"),
        YEAR191("19_1"),
        YEAR192("19_2"),
        YEAR201("20_1"),
        YEAR202("20_2"),
        YEAR211("21_1"),
        YEAR212("21_2"),
        YEAR221("22_1"),
        YEAR222("22_2");

        String year;

        ageGroup(String year) {
            this.year = year;
        }

        public String getYear() {
            return this.year;
        }
    }

    public Trainee(String lastName, String vorname, Habitat standort, Course.CourseName courseName, ageGroup ageGroup, int previousKnowledge) {
        super(lastName, vorname, standort);
        setCourse(courseName);
        setAgeGroup(ageGroup);
        setPreviousKnowledge(previousKnowledge);
    }

    public void setCourse(Course.CourseName courseName) {
        this.courseName = courseName;
    }

    public void setAgeGroup(ageGroup ageGroup){
        this.ageGroup = ageGroup;
    }

    public void setPreviousKnowledge(int previousKnowledge){
        if(previousKnowledge < 0 || previousKnowledge > 10){
            System.out.println("Javalevel muss zwischen 0 und 10 liegen");
        } else {
            this.previousKnowledge = previousKnowledge;
        }
    }

    public Course.CourseName getCourseName() {
        return courseName;
    }

    public ageGroup getAgeGroup() {
        return ageGroup;
    }

    public int getPreviousKnowledge() {
        return previousKnowledge;
    }

    @Override
    public String toString(){
        return super.toString() + " " + getCourseName() + " " + getAgeGroup() + " "  + getPreviousKnowledge();
    }
}
