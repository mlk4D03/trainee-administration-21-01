package com.trivadis.trainee.administration.model;

public class Person {
    private String lastName;
    private String firstName;
    private Habitat habitat;
    
    public Person(String lastName, String firstName, Habitat habitat){
        setLastName(lastName);
        setFirstName(firstName);
        setHabitat(habitat);
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }


    public String getLastName() {
        return lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public Habitat getHabitat() {
        return habitat;
    }

    public void setHabitat(Habitat habitat) {
        this.habitat = habitat;
    }

    @Override
    public String toString(){
        return getFirstName() + " " + getLastName() + " " + getHabitat();
    }
}
