package com.trivadis.trainee.administration.model;

public class Room {

    private int number;
    private Habitat habitat;
    private int maxPerson;

    public Room(int number, Habitat habitat, int maxPerson){
        setNumber(number);
        setCity(habitat);
        setMaxPerson(maxPerson);
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public void setCity(Habitat newHabitat) {
        habitat = newHabitat;
    }

    public void setMaxPerson(int maxPerson) {
        this.maxPerson = maxPerson;
    }

    public int getNumber() {
        return number;
    }

    public Habitat getCity() {
        return habitat;
    }

    public int getMaxPerson() {
        return maxPerson;
    }
}
