package com.trivadis.trainee.administration.model;

import java.util.ArrayList;

public class Course {

    private Room room;
    private ArrayList<Tutor> tutor;
    private CourseName courseName;
    private int number;
    private Habitat habitat;

    public enum CourseName {
        AZURE("Azure"),
        JAVA("Java"),
        DATENBANKEN("Datenbanken"),
        TESTING("Testing"),
        JAVAFX("JavaFX"),
        GIT("Git"),
        BI("BI"),
        SPRING("Spring");

        private String name;

        CourseName(String name) {
            this.name = name;
        }
    }

    public Course(Room room,  ArrayList<Tutor> tutor, CourseName courseName){
        setRoom(room);
        setTutor(tutor);
        setCourseName(courseName);
    }

    public Course(int number, CourseName courseName, Habitat habitat) {
        setNumber(number);
        setCourseName(courseName);
        setHabitat(habitat);
    }

    public Habitat getHabitat() {
        return habitat;
    }

    public void setHabitat(Habitat habitat) {
        this.habitat = habitat;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public void setTutor(ArrayList<Tutor> tutor) {
        this.tutor = tutor;
    }

    public void setCourseName(CourseName course) {
        courseName = course;
    }

    public Room getRoom() {
        return room;
    }

    public ArrayList<Tutor> getTutor() {
        return tutor;
    }

    public CourseName getCourseName() {
        return courseName;
    }

    @Override
    public String toString() {
        return "Course{" +
                "number=" + number +
                ", courseName=" + courseName +
                ", habitat=" + habitat +
                '}';
    }
}
