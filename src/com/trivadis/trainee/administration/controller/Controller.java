package com.trivadis.trainee.administration.controller;

import com.trivadis.trainee.administration.model.*;
import com.trivadis.trainee.administration.service.Service;
import com.trivadis.trainee.administration.view.Main;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.util.Callback;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Controller {

    @FXML
    public TableView<Person> traineeTable;

    @FXML
    public ComboBox<Label> chooseTableComboBox;

    @FXML
    public BorderPane borderPane;

    private TableView<Course> courseTable = new TableView<>();

    private Pane addAndEditTraineeView;

    private Pane addAndEditCourseView;

    private Service service=new Service();

    private final String COMBO_BOX_LABEL_TRAINEES = "Trainees";
    private final String COMBO_BOX_LABEL_COURSES = "Kurse";


    @FXML
    private void initialize() throws IOException {
        initializeComboBoxForSelectingTable();
        initializeTraineeTable();
        initializeCourseTable();
        initializeViews();
        initializeComboBoxes();
        addEventHandlerToTraineeSaveButton();
    }


    public void editRow(MouseEvent mouseEvent) throws IOException {
        Pane activeView;
        Person selectedTrainee = null;
        Course selectedCourse = null;
        if (this.isTraineeTableSelected()) {
            activeView = this.addAndEditTraineeView;
            selectedTrainee = this.traineeTable.getSelectionModel().getSelectedItem();
        } else {
            activeView = this.addAndEditCourseView;
            selectedCourse = this.courseTable.getSelectionModel().getSelectedItem();
        }
        if (!(selectedCourse == null && selectedTrainee == null)) {
            this.borderPane.setCenter(activeView);

            ObservableList<Node> viewList = activeView.getChildren();

            if (this.isTraineeTableSelected()) {
                this.populateTraineeFormWithData(viewList, selectedTrainee);
            }
        }
    }

    private void populateTraineeFormWithData(ObservableList<Node> viewList, Person selectedTrainee) {
        TextField firstName = getTextFieldWithIdFromView(this.addAndEditTraineeView,"textFieldFirstName");
        firstName.setText(selectedTrainee.getFirstName());

        TextField lastName = getTextFieldWithIdFromView(this.addAndEditTraineeView,"textFieldName");
        lastName.setText(selectedTrainee.getLastName());

        Slider previousKnowledge = getSliderFromView(this.addAndEditTraineeView);
        previousKnowledge.setValue(((Trainee) selectedTrainee).getPreviousKnowledge());

        ComboBox chooseYearComboBox = getComboBoxWithId(this.addAndEditTraineeView,"chooseYearComboBox");
        setSelectedComboBoxValue(((Trainee) selectedTrainee).getAgeGroup().name(),chooseYearComboBox);

        ComboBox chooseCourseComboBox = getComboBoxWithId(this.addAndEditTraineeView,"chooseCourseComboBox");
        setSelectedComboBoxValue(((Trainee) selectedTrainee).getCourseName().name(),chooseCourseComboBox);

        ComboBox choosePlaceComboBox = getComboBoxWithId(this.addAndEditTraineeView,"choosePlaceComboBox");
        setSelectedComboBoxValue(selectedTrainee.getHabitat().name(),choosePlaceComboBox);
    }

    /**
     * Befüllt eine übergebene ComboBox mit Werten.
     * @param labels die verschiedenen Werte, die auswählbar sein sollen.
     * @param comboBox die Combobox, die befüllt werden soll.
     */
    private void populateTraineeComboBoxWithData(List<String> labels, ComboBox comboBox){
        for(String s : labels) {
            Label newLabel = new Label(s);
            newLabel.setTextFill(Color.BLACK);
            comboBox.getItems().add(newLabel);
        }
    }

    /**
     * Belegt eine ComboBox mit einem Wert,
     * @param value der Wert, den eine Combobox hat.
     * @param comboBox die ComboBox, für die das geschehen soll.
     */
    private void setSelectedComboBoxValue(String value,ComboBox comboBox) {
        Label selectedLabel;
        ObservableList<Node> comboBoxChildren = comboBox.getItems();
        for(Node n : comboBoxChildren) {
            if(n instanceof Label) {
                selectedLabel = (Label) n;
                if(selectedLabel.getText().equalsIgnoreCase(value)) {
                    comboBox.setValue(selectedLabel);
                    break;
                }
            }
        }
    }

    private void addEventHandlerToTraineeSaveButton() {
        ObservableList<Node> viewList = this.addAndEditTraineeView.getChildren();
        for(Node n: viewList) {
            if(n instanceof Button) {
                Button saveButton = (Button) n;
                if(saveButton.getId().equalsIgnoreCase("buttonAddToList")) {
                    saveButton.setOnMouseClicked(mouseEvent -> {
                        String firstName = getTextFieldWithIdFromView(addAndEditTraineeView,"textFieldFirstname").getText();
                        String lastName = getTextFieldWithIdFromView(addAndEditTraineeView,"textFieldName").getText();
                        int skill = (int) getSliderFromView(addAndEditTraineeView).getValue();
                        String habitat = ((Label)getComboBoxWithId(addAndEditTraineeView,"choosePlaceComboBox").getValue()).getText().toUpperCase();
                        String course = ((Label)getComboBoxWithId(addAndEditTraineeView,"chooseCourseComboBox").getValue()).getText().toUpperCase();
                        String year = ((Label)getComboBoxWithId(addAndEditTraineeView,"chooseYearComboBox").getValue()).getText().toUpperCase();
                        Person newPerson = new Trainee(lastName,firstName,Habitat.valueOf(habitat),Course.CourseName.valueOf(course), Trainee.ageGroup.valueOf(year),skill);
                        System.out.println(newPerson);
                        // TODO insert-Statement aufrufen - vorher prüfen ob neu oder bearbeitet

                    });
                }
            }
        }
    }

    public void deleteRow(MouseEvent mouseEvent) {

    }

    public void addRow(MouseEvent mouseEvent) {
        //this.borderPane.setCenter(this.addAndEditTraineeView);
        traineeTable.getItems().add(new Trainee("Döffinger", "Mika", Habitat.HAMBURG, Course.CourseName.JAVA, Trainee.ageGroup.YEAR211, 7));
        traineeTable.getItems().add(new Trainee("Koch", "Anette", Habitat.MANNHEIM, Course.CourseName.JAVA, Trainee.ageGroup.YEAR211, 7));
        traineeTable.getItems().add(new Trainee("Heer", "Phillip", Habitat.DUESSELDORF, Course.CourseName.JAVA, Trainee.ageGroup.YEAR211, 7));
        traineeTable.getItems().add(new Trainee("Friedrich", "Jimmy", Habitat.BERN, Course.CourseName.JAVA, Trainee.ageGroup.YEAR211, 7));
        traineeTable.getItems().add(new Trainee("Grioui", "Amine", Habitat.STUTTGART, Course.CourseName.JAVA, Trainee.ageGroup.YEAR211, 7));
    }

    /**
     * Gibt an, ob die Trainee-Tabelle momentan ausgewählt ist.
     *
     * @return true wenn ja, false wenn die Kurstabelle ausgewählt ist.
     */
    private boolean isTraineeTableSelected() {
        return (this.chooseTableComboBox.getValue().getText().equals(this.COMBO_BOX_LABEL_TRAINEES));
    }

    /**
     * Updatet eine Combobox, nachdem ein Item ausgewählt wurde.
     * Benötigt man, damit bei einer ComboBox die Items nicht verschwinden, nachdem
     * diese ausgewählt wurden.
     * @return Callback zum Updaten der ComboBox
     */
    private Callback<ListView<Label>, ListCell<Label>> createCallBackForUpdatingComboBox() {
        Callback<ListView<Label>, ListCell<Label>> cellFactory = new Callback<ListView<Label>, ListCell<Label>>() {

            @Override
            public ListCell<Label> call(ListView<Label> l) {
                return new ListCell<Label>() {

                    @Override
                    protected void updateItem(Label item, boolean empty) {
                        super.updateItem(item, empty);
                        if (item == null || empty) {
                            setGraphic(null);
                        } else {
                            setText(item.getText());
                        }
                    }
                };
            }
        };

        return cellFactory;
    }

    /**
     * Initialisiert die ComboBox auf der Hauptseite mit den auswählbaren Tabellen.
     */
    private void initializeComboBoxForSelectingTable() {
        Label traineeLabel = new Label(COMBO_BOX_LABEL_TRAINEES);
        Label courseLabel = new Label(COMBO_BOX_LABEL_COURSES);

        traineeLabel.setTextFill(Color.BLACK);
        courseLabel.setTextFill(Color.BLACK);

        this.chooseTableComboBox.getItems().addAll(traineeLabel, courseLabel);
        this.chooseTableComboBox.setValue(traineeLabel);

        this.chooseTableComboBox.setCellFactory(createCallBackForUpdatingComboBox());
    }

    /**
     * Initialisiert die Traineetabelle mit allen notwendigen Spalten und Properties.
     */
    private void initializeTraineeTable() {
        TableColumn<Person, String> column1 = new TableColumn<>("Vorname");
        column1.setCellValueFactory(new PropertyValueFactory<>("firstName"));

        TableColumn<Person, String> column2 = new TableColumn<>("Nachname");
        column2.setCellValueFactory(new PropertyValueFactory<>("lastName"));

        TableColumn<Person, Habitat> column3 = new TableColumn<>("Standort");
        column3.setCellValueFactory(new PropertyValueFactory<>("habitat"));

        TableColumn<Person, Course.CourseName> column4 = new TableColumn<>("Kurs");
        column4.setCellValueFactory(new PropertyValueFactory<>("courseName"));

        TableColumn<Person, Trainee.ageGroup> column5 = new TableColumn<>("Jahrgang");
        column5.setCellValueFactory(new PropertyValueFactory<>("ageGroup"));

        TableColumn<Person, Integer> column6 = new TableColumn<>("Vorkenntnisse");
        column6.setCellValueFactory(new PropertyValueFactory<>("previousKnowledge"));

        traineeTable.getColumns().add(column1);
        traineeTable.getColumns().add(column2);
        traineeTable.getColumns().add(column3);
        traineeTable.getColumns().add(column4);
        traineeTable.getColumns().add(column5);
        traineeTable.getColumns().add(column6);

        traineeTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
    }

    /**
     * Initialisiert die Kurstabelle mit allen notwendigen Spalten und Properties.
     */
    private void initializeCourseTable() {
        TableColumn<Course, Course.CourseName> column1 = new TableColumn<>("Kursname");
        column1.setCellValueFactory(new PropertyValueFactory<>("courseName"));

        TableColumn<Course, Room> column2 = new TableColumn<>("Raum");
        column2.setCellValueFactory(new PropertyValueFactory<>("number"));

        this.courseTable.getColumns().add(column1);
        this.courseTable.getColumns().add(column2);
    }

    /**
     * Zeigt eine ausgewählte Tabelle in der Mitte der Anwendung an.
     * @param mouseEvent Click auf den Anzeige-Button auf der Oberfläche.
     */
    public void showTable(MouseEvent mouseEvent) {
        switch (this.chooseTableComboBox.getValue().getText()) {
            case "Trainees":
                this.borderPane.setCenter(this.traineeTable);
                break;
            case "Kurse":
                this.borderPane.setCenter(this.courseTable);
                break;
            default:
        }
    }

    /**
     * Initialisiert die verschiedenen Ansichten für das Hinzufügen/Editieren
     * einer Trainee/Kurs Tabelle.
     * @throws IOException Exception, falls Datei nicht gefunden werden kann.
     */
    private void initializeViews() throws IOException {
        this.addAndEditTraineeView = FXMLLoader.load(Main.class.getResource("addOrEditTrainee.fxml"));
        this.addAndEditCourseView = FXMLLoader.load(Main.class.getResource("addOrEditCourse.fxml"));
    }

    private void intitializeHabitatComboBox(){
        ComboBox choosePlaceComboBox = getComboBoxWithId(addAndEditTraineeView,"choosePlaceComboBox");
        ArrayList<String> habitats = new ArrayList<>(); // TODO DB-Abfrage benutzen
        habitats.add("Hamburg");
        habitats.add("Duesseldorf");
        habitats.add("Frankfurt");
        habitats.add("Mannheim");
        habitats.add("München");
        habitats.add("Stuttgart");
        habitats.add("Wien");
        habitats.add("Bern");
        habitats.add("Zürich");
        habitats.add("Genf");
        habitats.add("Freiburg");
        habitats.add("Lausanne");
        populateTraineeComboBoxWithData(habitats,choosePlaceComboBox);
        choosePlaceComboBox.setCellFactory(createCallBackForUpdatingComboBox());
    }

    private void initializeCourseComboBox() {
        ComboBox chooseCourseComboBox = getComboBoxWithId(this.addAndEditTraineeView,"chooseCourseComboBox");
        ArrayList<String> course = new ArrayList<>(); // TODO DB-Abfrage benutzen
        course.add("JAVA");
        course.add("AZURE");
        course.add("GIT");
        course.add("BI");
        course.add("SPRING BOOT");
        populateTraineeComboBoxWithData(course,chooseCourseComboBox);
        chooseCourseComboBox.setCellFactory(createCallBackForUpdatingComboBox());
    }

    private void initializeYearComboBox() {
        ComboBox chooseYearComboBox = getComboBoxWithId(this.addAndEditTraineeView,"chooseYearComboBox");
        ArrayList<String> years = new ArrayList<>(); // TODO DB-Abfrage benutzen
        years.add("YEAR181");
        years.add("YEAR182");
        years.add("YEAR191");
        years.add("YEAR192");
        years.add("YEAR201");
        years.add("YEAR202");
        years.add("YEAR211");
        populateTraineeComboBoxWithData(years,chooseYearComboBox);
        chooseYearComboBox.setCellFactory(createCallBackForUpdatingComboBox());
    }

    private void initializeComboBoxes() {
        initializeCourseComboBox();
        intitializeHabitatComboBox();
        initializeYearComboBox();
    }

    private ComboBox getComboBoxWithId(Pane view,String id) {
        ObservableList<Node> childrens = view.getChildren();
        for(Node n : childrens) {
            if(n instanceof ComboBox) {
                ComboBox comboBox = (ComboBox) n;
                if(comboBox.getId().equals(id)) {
                   return comboBox;
                }
            }
        }
        return null;
    }

    private TextField getTextFieldWithIdFromView(Pane view, String id) {
        ObservableList<Node> childrens = view.getChildren();
        for(Node n : childrens) {
            if(n instanceof TextField)  {
                TextField textField = (TextField) n;
                if(textField.getId().equalsIgnoreCase(id)) {
                    return textField;
                }
            }
        }
        return null;
    }

    private Slider getSliderFromView(Pane view) {
        ObservableList<Node> childrens = view.getChildren();
        for(Node n : childrens) {
            if(n instanceof Slider) {
                return (Slider) n;
            }
        }
        return null;
    }

    private void clearTable(TableView table) {
        table.getItems().clear();
    }

    private void fillTraineeTable(){
        // TODO DB-Abfrage alle Trainees holen und die Tabelle fuellen - vorher clearTable aufrufen
    }

}