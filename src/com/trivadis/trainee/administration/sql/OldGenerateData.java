package com.trivadis.trainee.administration.sql;

import com.trivadis.trainee.administration.model.*;
import com.trivadis.trainee.administration.service.Service;

import java.sql.*;
import java.util.*;
import java.util.stream.Collectors;

public class OldGenerateData {

    Service service = new Service();

    public void generateData() {

        try{
            Statement stmt = service.getService();
            String dbName = "trainee_administration";
            String sql = "CREATE DATABASE " + dbName;
            stmt.executeUpdate(sql);

            stmt = service.getService();
            sql = "CREATE TABLE HABITAT " +
                    "(habitat VARCHAR(255) NOT NULL, " +
                    " street VARCHAR(255) NOT NULL, " +
                    " number VARCHAR(255) NOT NULL , " +
                    " zip VARCHAR(255) NOT NULL, " +
                    " PRIMARY KEY ( habitat ))";
            stmt.executeUpdate(sql);

            sql = "CREATE TABLE ROOM " +
                    "(number INTEGER NOT NULL, " +
                    " maxPerson INTEGER NOT NULL, " +
                    " habitat VARCHAR(255) NOT NULL, " +
                    " PRIMARY KEY ( number, habitat ), " +
                    " FOREIGN KEY ( habitat ) REFERENCES HABITAT ( habitat ))";
            stmt.executeUpdate(sql);

            sql = "CREATE TABLE COURSE " +
                    "(courseName VARCHAR(255) NOT NULL, " +
                    " room INTEGER NOT NULL, " +
                    " habitat VARCHAR(255) NOT NULL, " +
                    " PRIMARY KEY ( courseName, room, habitat ), " +
                    " FOREIGN KEY ( room ) REFERENCES ROOM ( number ))";
            stmt.executeUpdate(sql);

            sql = "CREATE TABLE TRAINEE " +
                    "(traineeId INTEGER NOT NULL AUTO_INCREMENT, " +
                    " lastName VARCHAR(255) NOT NULL , " +
                    " firstName VARCHAR(255) NOT NULL, " +
                    " habitat VARCHAR(255) NOT NULL, " +
                    " courseName VARCHAR(255) NOT NULL, " +
                    " ageGroup VARCHAR(255) NOT NULL, " +
                    " previousKnowledge INTEGER NOT NULL, " +
                    " PRIMARY KEY ( traineeId ), " +
                    " FOREIGN KEY ( habitat ) REFERENCES HABITAT ( habitat ), " +
                    " FOREIGN KEY ( courseName ) REFERENCES COURSE ( courseName ))";
            stmt.executeUpdate(sql);



            stmt = service.getService();
            List<Address> addresses = createTrivadisAddresses();
            for (Address address : addresses) {
                String query = String.format("INSERT INTO HABITAT (habitat, street, number, zip) VALUES ('%s', '%s', '%s', '%s')", address.getHabitat(), address.getStreet(), address.getNumber(), address.getZip());
                stmt.executeUpdate(query);
            }

            int quantity = 10;
            List<Room> rooms = createRandomRooms(quantity);
            for (Room r : rooms) {
                String query = String.format("INSERT INTO ROOM VALUES (%d, %d, '%s')", r.getNumber(), r.getMaxPerson(), r.getCity());
                stmt.executeUpdate(query);
            }


            List<Course> courses = createTrivadisCourses(quantity);
            for (Course c : courses) {
                String query = String.format("INSERT INTO COURSE VALUES ('%s', %d, '%s')", c.getCourseName(), c.getNumber(), c.getHabitat());
                stmt.executeUpdate(query);
            }



            stmt = service.getService();
            sql = "SELECT courseName FROM COURSE";
            ResultSet rs = stmt.executeQuery(sql);
            List<String> courseNames = new ArrayList<>();
            while(rs.next()){
                String courseName = rs.getString("courseName");
                courseNames.add(courseName);
            }
            courseNames = courseNames.stream()
                    .distinct()
                    .collect(Collectors.toList());
            System.out.println(courseNames);


            stmt = service.getService();
            sql = "SELECT * FROM COURSE";
            rs = stmt.executeQuery(sql);
            List<String> allCourses = new ArrayList<>();
            while(rs.next()){
                String courseName = rs.getString("courseName");
                int room = rs.getInt("room");
                String habitat = rs.getString("habitat");
                allCourses.add("Coursename: " + courseName + ", in room: " + room + ", @Location: " + habitat);
            }
            System.out.println(allCourses);


            /*
            // Alle Trainees in einer Liste
            stmt = con.createStatement();
            sql = "SELECT lastName, firstName FROM TRAINEE";
            rs = stmt.executeQuery(sql);
            List<String> allTrainees = new ArrayList<>();
            while(rs.next()){
                String firstName = rs.getString("firstName");
                String lastName = rs.getString("lastName");
                allTrainees.add(firstName + " " + lastName);
            }
            System.out.println(allTrainees);



            stmt = con.createStatement();
            sql = "SELECT ageGroup FROM TRAINEE";
            rs = stmt.executeQuery(sql);
            List<String> allAgeGroup = new ArrayList<>();
            while(rs.next()){
                String ageGroup = rs.getString("ageGroup");
                allAgeGroup.add(ageGroup);
            }

            allAgeGroup = allAgeGroup.stream()
                    .distinct()
                    .collect(Collectors.toList());
            System.out.println(allAgeGroup);

             */



            rs.close();







            //tbc


            /*
            stmt = con.createStatement();
            sql = "CREATE TABLE REGISTRATION " +
                    "(id INTEGER not NULL, " +
                    " first VARCHAR(255), " +
                    " last VARCHAR(255), " +
                    " department VARCHAR(255), " +
                    " passOutYear INTEGER, " +
                    " age INTEGER, " +
                    " isTrinkfest BIT, " +
                    " PRIMARY KEY ( id ))";
            stmt.executeUpdate(sql);
            sql = "CREATE TABLE ORDERBOOK " +
                    "(OrderID INTEGER not NULL, " +
                    " OrderNumber INTEGER, " +
                    " AngstVorEnten BIT, " +
                    " id INTEGER not NULL, " +
                    " PRIMARY KEY ( OrderID ), " +
                    " FOREIGN KEY ( id ) REFERENCES REGISTRATION( id ))";
            stmt.executeUpdate(sql);

            stmt = con.createStatement();
            int anzahl = 1000;
            List<Student> students = createRandomStudentList(anzahl);
            for (Student s : students) {
                String query = String.format("INSERT INTO Registration VALUES (%d, '%s', '%s', '%s', %d, %d, %b)", s.getId(), s.getFirst(), s.getLast(), s.getDepartment(), s.getPassOutYear(), s.getAge(), s.isTrinkfest());
                stmt.executeUpdate(query);
            }
            int anzahl2 = 100;
            List<Order> orders = createRandomOrderList(anzahl2);
            for (Order o : orders) {
                String query = String.format("INSERT INTO ORDERBOOK VALUES (%d, %d, %b, %d)", o.getOrderID(), o.getOrderNumber(), o.isAngstVorEnten(), o.getId());
                stmt.executeUpdate(query);
            }

            stmt = con.createStatement();
            sql = "SELECT Registration.id, Registration.first, Registration.age, Registration.isTrinkfest, Orderbook.id, Orderbook.AngstVorEnten FROM Registration INNER JOIN Orderbook ON Registration.id=Orderbook.id WHERE isTrinkfest = 1 AND age BETWEEN 20 AND 50 AND AngstVorEnten = 1";
            ResultSet rs = stmt.executeQuery(sql);
            int count = 0;
            while(rs.next()){
                int age = rs.getInt("age");
                String first = rs.getString("first");
                boolean isTrinkfest = rs.getBoolean("isTrinkfest");
                boolean hasAngstVorEnten = rs.getBoolean("AngstVorEnten");
                System.out.println(first + ", " + age + ": isTrinkfest=" + isTrinkfest + " and hasAngstVorEnten=" + hasAngstVorEnten);
                count++;
            }
            System.out.println(count);
            rs.close();

             */

        }catch(SQLException e){
            e.printStackTrace();
        }catch(Exception e){
            e.printStackTrace();
            e.getCause();
        }
        System.out.println("Goodbye!");
    }

    private static List<Address> createTrivadisAddresses() {

        List<String> trivadisStreets = Arrays.asList(
                "Elisabethenanlage",
                "Weltpoststrasse",
                "Badenerstrasse",
                "George Constantinescu Street Globalworth Campus, B building, 1st floor",
                "Werdener Strasse",
                "Atricom, Lyoner Strasse",
                "Heinrich-von-Stephan-Str.",
                "Route de Peney, Postfach 678",
                "Paul-Dessau-Strasse , Gebäude P",
                "Rue Marterey",
                "Weinheimer Strasse",
                "Riem Arcaden/Neue Messe, Lehrer-Wirth-Strasse",
                "Industriestrasse",
                "Gertrude-Fröhlich-Sandner-Straße, QBC 1, Top 7, 2. OG",
                "Sägereistrasse");
        List<String> trivadisZIP = Arrays.asList("4051", "3015", "5200", "020339", "40227","60528", "79100", "1214", "22761", "1005", "68309","81829", "70565", "1100","8152");
        List<String> trivadisHouseNumber = Arrays.asList("9", "5", "13", "2-4B", "4", "15", "17", "2", "6", "5", "68", "4", "4", "1", "29");
        List<String> trivadisLocation = Arrays.asList("BASEL", "BERN", "BRUGG", "BUKAREST", "DUESSELDORF", "FRANKFURT_AM_MAIN",
                "FREIBURG", "GENF", "HAMBURG", "LAUSANNE", "MANNHEIM", "MUENCHEN", "STUTTGART", "WIEN", "ZUERICH");

        List<Address> addresses = new ArrayList<>();
        for (int i = 0; i < trivadisStreets.size(); i++) {

            addresses.add(new Address(trivadisStreets.get(i), trivadisZIP.get(i), trivadisHouseNumber.get(i), Habitat.valueOf(trivadisLocation.get(i))));

        }

        return addresses;
    }

    private static Habitat randomHabitat() {
        int pick = new Random().nextInt(Habitat.values().length);
        return Habitat.values()[pick];
    }

    public static List<Room> createRandomRooms(int numbers) {

        Random r = new Random();
        int low = 50;
        int high = 200;

        List<Room> rooms = new ArrayList<>();
        for (int i = 0; i < numbers; i++) {
            int randomMax = r.nextInt(high-low) + low;
            Habitat h = randomHabitat();
            rooms.add(new Room(i, h, randomMax));
        }

        return rooms;

    }

    private static Course.CourseName randomCourse() {
        int pick = new Random().nextInt(Course.CourseName.values().length);
        return Course.CourseName.values()[pick];
    }

    public static List<Course> createTrivadisCourses(int numbers) {
        List<Course> courses = new ArrayList<>();
        for (int i = 0; i < numbers; i++) {
            Course.CourseName c = randomCourse();
            Habitat h = randomHabitat();
            courses.add(new Course(i, c, h));
        }

        return courses;

    }

    public static List<Trainee> createRandomTrainees(int numbers) {

        return null;

    }









    public static List<Trainee> createRandomStudentList(int numbers) {

        List<String> first = Arrays.asList("Anton", "Berta", "Cyril", "Dario", "Emil", "Fritz", "Gustavo", "Heinz", "Ian", "Jeffrey", "Guido", "Larissa", "Susanne", "Stefan", "Benjamin", "Lorena", "Samantha", "Gregory");
        List<String> last = Arrays.asList("Müller", "Schmidt", "Schneider", "Fischer", "Weber", "Schäfer", "Hartmann", "Weiss", "Hahn", "Maradona", "Seinfeld", "Brantschen", "Vogel", "Beck", "Lorenz", "Seidel", "Schreiber", "Bergmann");
        List<String> department = Arrays.asList("Management", "Law", "Engineering", "Science", "Statistics", "Physics", "Industrial");
        int minPassOutYear = 2000;
        int maxPassOutYear = 2020;

        Random r = new Random();

        List<Trainee> persons = new ArrayList<>();
        for (int i = 0; i < numbers; i++) {

            String sVorname = first.get(r.nextInt(first.size()));
            String sNachname = last.get(r.nextInt(last.size()));
            String sDepartment = department.get(r.nextInt(department.size()));
            int rPassOutYear = r.nextInt(maxPassOutYear - minPassOutYear) + minPassOutYear;
            int rAlter = r.nextInt(100);
            boolean rBoolean = r.nextBoolean();

            // persons.add(new Student((long) i, sVorname, sNachname, sDepartment, rPassOutYear, rAlter, rBoolean));
        }

        return persons;
    }

    /*
    public static List<Order> createRandomOrderList(int numbers) {

        int minOrderNumber = 10000;
        int maxOrderNumber = 20000;

        Random r = new Random();

        // get distinct Integer Set and convert it to int array
        Set<Integer> set = new LinkedHashSet<>();
        while (set.size() < numbers) {
            set.add(r.nextInt(numbers));
        }
        int[] primitive = set.stream()
                .mapToInt(Integer::intValue)
                .toArray();

        List<Order> orders = new ArrayList<>();
        for (int i = 0; i < numbers; i++) {
            int rOrderNumber = r.nextInt(maxOrderNumber - minOrderNumber) + minOrderNumber;
            boolean rBoolean = r.nextBoolean();
            int rID = r.nextInt(100);

            orders.add(new Order((long) i, rOrderNumber, rBoolean, (long) primitive[i]));

        }

        return orders;


    }*/
}