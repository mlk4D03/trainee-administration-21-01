package com.trivadis.trainee.administration.sql;

import com.trivadis.trainee.administration.service.Service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class InsertTraineeData {

    Service service = new Service();

    public void insertTraineeData() {
        try{
            Statement stmt = service.getService();
            List<String> trainees = new ArrayList<>();
            trainees.add("INSERT INTO TRAINEE (lastName, firstName, habitat, courseName, ageGroup, previousKnowledge) VALUES ('Grioui', 'Amine', 'STUTTGART', 'JAVA', 'YEAR211', '7')");
            trainees.add("INSERT INTO TRAINEE (lastName, firstName, habitat, courseName, ageGroup, previousKnowledge) VALUES ('Koch', 'Anette', 'MANNHEIM', 'JAVA', 'YEAR211', '7')");
            trainees.add("INSERT INTO TRAINEE (lastName, firstName, habitat, courseName, ageGroup, previousKnowledge) VALUES ('Friedrich', 'Jimmy', 'BERN', 'JAVA', 'YEAR211', '7')");
            trainees.add("INSERT INTO TRAINEE (lastName, firstName, habitat, courseName, ageGroup, previousKnowledge) VALUES ('Döffinger', 'Mika', 'HAMBURG', 'JAVA', 'YEAR211', '7')");
            trainees.add("INSERT INTO TRAINEE (lastName, firstName, habitat, courseName, ageGroup, previousKnowledge) VALUES ('Heer', 'Phillip', 'DUESSELDORF', 'JAVA', 'YEAR211', '7')");
            for (String t : trainees) {
                stmt.executeUpdate(t);
            }
            System.out.println("Trainee records inserted successfully...");

        }catch(SQLException e){
            e.printStackTrace();
        }catch(Exception e){
            e.printStackTrace();
            e.getCause();
        }
        System.out.println("Goodbye!");
    }
}