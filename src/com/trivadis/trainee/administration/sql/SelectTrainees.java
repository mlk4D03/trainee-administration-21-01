package com.trivadis.trainee.administration.sql;

import com.trivadis.trainee.administration.service.Service;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class SelectTrainees {

    Service service = new Service();

    public List<String> selectTrainee() {
        List<String> allTrainees = new ArrayList<>();
        try {
            Statement stmt = service.getService();
            String sql = "SELECT lastName, firstName FROM TRAINEE";
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                String firstName = rs.getString("firstName");
                String lastName = rs.getString("lastName");
                allTrainees.add(firstName + " " + lastName);
            }
            System.out.println(allTrainees);
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
            e.getCause();
        }
        System.out.println("Goodbye!");
        return allTrainees;
    }

}