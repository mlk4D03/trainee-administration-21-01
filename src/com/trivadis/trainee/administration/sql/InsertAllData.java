package com.trivadis.trainee.administration.sql;

import com.trivadis.trainee.administration.model.*;
import com.trivadis.trainee.administration.service.Service;
import java.sql.*;
import java.util.List;

public class InsertAllData {

    Service service = new Service();

    public void insertAllData() {
        try {
            Statement stmt = service.getService();
            List<Address> addresses = AllInsertMethods.createTrivadisAddresses();
            for (Address address : addresses) {
                String query = String.format("INSERT INTO HABITAT (habitat, street, number, zip) VALUES ('%s', '%s', '%s', '%s')", address.getHabitat(), address.getStreet(), address.getNumber(), address.getZip());
                stmt.executeUpdate(query);
            }
            int quantity = 10;
            List<Room> rooms = AllInsertMethods.createRandomRooms(quantity);
            for (Room r : rooms) {
                String query = String.format("INSERT INTO ROOM VALUES (%d, %d, '%s')", r.getNumber(), r.getMaxPerson(), r.getCity());
                stmt.executeUpdate(query);
            }
            List<Course> courses = AllInsertMethods.createTrivadisCourses(quantity);
            for (Course c : courses) {
                String query = String.format("INSERT INTO COURSE VALUES ('%s', %d, '%s')", c.getCourseName(), c.getNumber(), c.getHabitat());
                stmt.executeUpdate(query);
            }
            System.out.println("Records inserted successfully...");
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
            e.getCause();
        }
        System.out.println("Goodbye!");
    }
}