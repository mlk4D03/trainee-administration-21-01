package com.trivadis.trainee.administration.sql;

import com.trivadis.trainee.administration.service.Service;
import java.sql.SQLException;
import java.sql.Statement;

public class CreateAllTables {

    Service service = new Service();

    public void createAllTables(){
        try{
            Statement stmt = service.getService();
            String sql = "CREATE TABLE HABITAT " +
                    "(habitat VARCHAR(255) NOT NULL, " +
                    " street VARCHAR(255) NOT NULL, " +
                    " number VARCHAR(255) NOT NULL , " +
                    " zip VARCHAR(255) NOT NULL, " +
                    " PRIMARY KEY ( habitat ))";
            stmt.executeUpdate(sql);

            sql = "CREATE TABLE ROOM " +
                    "(number INTEGER NOT NULL, " +
                    " maxPerson INTEGER NOT NULL, " +
                    " habitat VARCHAR(255) NOT NULL, " +
                    " PRIMARY KEY ( number, habitat ), " +
                    " FOREIGN KEY ( habitat ) REFERENCES HABITAT ( habitat ))";
            stmt.executeUpdate(sql);

            sql = "CREATE TABLE COURSE " +
                    "(courseName VARCHAR(255) NOT NULL, " +
                    " room INTEGER NOT NULL, " +
                    " habitat VARCHAR(255) NOT NULL, " +
                    " PRIMARY KEY ( courseName, room, habitat ), " +
                    " FOREIGN KEY ( room ) REFERENCES ROOM ( number ))";
            stmt.executeUpdate(sql);

            sql = "CREATE TABLE TRAINEE " +
                    "(traineeId INTEGER NOT NULL AUTO_INCREMENT, " +
                    " lastName VARCHAR(255) NOT NULL , " +
                    " firstName VARCHAR(255) NOT NULL, " +
                    " habitat VARCHAR(255) NOT NULL, " +
                    " courseName VARCHAR(255) NOT NULL, " +
                    " ageGroup VARCHAR(255) NOT NULL, " +
                    " previousKnowledge INTEGER NOT NULL, " +
                    " PRIMARY KEY ( traineeId ), " +
                    " FOREIGN KEY ( habitat ) REFERENCES HABITAT ( habitat ), " +
                    " FOREIGN KEY ( courseName ) REFERENCES COURSE ( courseName ))";
            stmt.executeUpdate(sql);

            System.out.println("Tables created successfully...");
        }catch(SQLException e){
            e.printStackTrace();
        }catch(Exception e){
            e.printStackTrace();
            e.getCause();
        }
        System.out.println("Goodbye!");
    }
}