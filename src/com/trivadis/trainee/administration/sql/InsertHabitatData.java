package com.trivadis.trainee.administration.sql;

import com.trivadis.trainee.administration.model.Address;
import com.trivadis.trainee.administration.service.Service;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public class InsertHabitatData {

    Service service = new Service();

    public void insertHabitatData() {
        try{
            Statement stmt = service.getService();
            List<Address> addresses = AllInsertMethods.createTrivadisAddresses();
            for (Address address : addresses) {
                String query = String.format("INSERT INTO HABITAT (habitat, street, number, zip) VALUES ('%s', '%s', '%s', '%s')", address.getHabitat(), address.getStreet(), address.getNumber(), address.getZip());
                stmt.executeUpdate(query);
            }

            System.out.println("Habitat records inserted successfully...");

        }catch(SQLException e){
            e.printStackTrace();
        }catch(Exception e){
            e.printStackTrace();
            e.getCause();
        }
        System.out.println("Goodbye!");
    }
}