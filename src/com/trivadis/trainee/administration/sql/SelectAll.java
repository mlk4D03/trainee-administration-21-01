package com.trivadis.trainee.administration.sql;


import com.trivadis.trainee.administration.model.Course;
import com.trivadis.trainee.administration.model.Habitat;
import com.trivadis.trainee.administration.model.Trainee;
import com.trivadis.trainee.administration.service.Service;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class SelectAll {

    Service service = new Service();

    /**
     * @return a list of trainee
     */
    /**
     * @return a list of trainee
     */
    public List<?> selectAll(String tableName) {
        List<Trainee> traineeList = new ArrayList<>();
        List<Course> courseList = new ArrayList<>();
        try {
            Statement stmt = service.getService();
            if (tableName.equals("Trainee")) {
                String sql = "SELECT * FROM Trainee";
                ResultSet rs = stmt.executeQuery(sql);
                while (rs.next()) {
                    String lastName = rs.getString("lastName");
                    String firstName = rs.getString("firstName");
                    int previousKnowledge = rs.getInt("previousKnowledge");
                    String ageGroup = rs.getString("ageGroup");
                    String habitat = rs.getString("habitat");
                    String courseName = rs.getString("courseName");

                    Trainee trainee = new Trainee(lastName, firstName, Habitat.valueOf(habitat), Course.CourseName.valueOf(courseName.toUpperCase()), Trainee.ageGroup.valueOf(ageGroup), previousKnowledge);
                    traineeList.add(trainee);
                }
                rs.close();
            } else if (tableName.equals("Course")) {
                String sql = "SELECT * FROM COURSE";
                ResultSet rs = stmt.executeQuery(sql);
                while (rs.next()) {
                    int room = rs.getInt("room");
                    String habitat = rs.getString("habitat");
                    String courseName = rs.getString("courseName");
                    Course course = new Course(room, Course.CourseName.valueOf(courseName.toUpperCase()), Habitat.valueOf(habitat));
                    courseList.add(course);
                }
                rs.close();
            }
        } catch (SQLException se) {
            se.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return traineeList.isEmpty() ? courseList : traineeList;
    }
}
