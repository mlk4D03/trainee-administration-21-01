package com.trivadis.trainee.administration.sql;

import com.trivadis.trainee.administration.model.Course;
import com.trivadis.trainee.administration.service.Service;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public class InsertCourseData {

    Service service = new Service();

    public void insertCourseData() {
        try {
            Statement stmt = service.getService();
            int quantity = 10;
            List<Course> courses = AllInsertMethods.createTrivadisCourses(quantity);
            for (Course c : courses) {
                String query = String.format("INSERT INTO COURSE VALUES ('%s', %d, '%s')", c.getCourseName(), c.getNumber(), c.getHabitat());
                stmt.executeUpdate(query);
            }
            System.out.println("Course records inserted successfully...");
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
            e.getCause();
        }
        System.out.println("Goodbye!");
    }
}