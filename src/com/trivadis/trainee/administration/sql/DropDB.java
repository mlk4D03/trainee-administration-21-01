package com.trivadis.trainee.administration.sql;//STEP 1. Import required packages

import com.trivadis.trainee.administration.service.Service;
import java.sql.SQLException;
import java.sql.Statement;

public class DropDB {
    Service service = new Service();

    public void dropDB() {
        try {
            Statement stmt = service.getService();
            String sql = "DROP DATABASE trainee_administration";
            stmt.executeUpdate(sql);
            System.out.println("Database deleted successfully...");
        } catch (SQLException se) {
            se.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("Goodbye!");
    }
}