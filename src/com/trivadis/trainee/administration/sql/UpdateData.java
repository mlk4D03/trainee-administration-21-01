package com.trivadis.trainee.administration.sql;

import com.trivadis.trainee.administration.service.Service;

import java.sql.SQLException;
import java.sql.Statement;


public class UpdateData {

    private Service service = new Service();

    //field auch als String möglich
    public void updateData(String field, String value, int id) throws SQLException {
        Statement stmt = service.getService();
        String query = String.format("UPDATE TRAINEE SET %s = '%s' where traineeID = %d", field, value, id);
        stmt.executeUpdate(query);
    }
}
