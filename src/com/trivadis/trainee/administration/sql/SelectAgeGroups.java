package com.trivadis.trainee.administration.sql;

import com.trivadis.trainee.administration.service.Service;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class SelectAgeGroups {

    Service service = new Service();

    public List<String> SelectAgeGroup() {
        List<String> allAgeGroup = new ArrayList<>();
        try{
            Statement stmt = service.getService();
            String sql = "SELECT ageGroup FROM TRAINEE";
            ResultSet rs = stmt.executeQuery(sql);
            while(rs.next()){
                String ageGroup = rs.getString("ageGroup");
                allAgeGroup.add(ageGroup);
            }

            allAgeGroup = allAgeGroup.stream()
                    .distinct()
                    .collect(Collectors.toList());
            System.out.println(allAgeGroup);

            rs.close();

        }catch(SQLException e){
            e.printStackTrace();
        }catch(Exception e){
            e.printStackTrace();
            e.getCause();
        }
        System.out.println("Goodbye!");
        return allAgeGroup;
    }

}