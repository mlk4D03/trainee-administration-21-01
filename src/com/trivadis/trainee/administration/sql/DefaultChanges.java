package com.trivadis.trainee.administration.sql;

public class DefaultChanges {

    private static final String USER = "trainee";
    private static final String PW = "Igel1234";
    private static final String MY_SQL_DRIVER = "com.mysql.cj.jdbc.Driver";

    public static String getUSER() {
        return USER;
    }

    public static String getPW() {
        return PW;
    }

    public static String getMySqlDriver() {
        return MY_SQL_DRIVER;
    }
}
