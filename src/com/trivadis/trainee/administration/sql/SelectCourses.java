package com.trivadis.trainee.administration.sql;

import com.trivadis.trainee.administration.service.Service;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class SelectCourses {

    Service service = new Service();

    public List<String> selectCourses(){
        List<String> allCourses = new ArrayList<>();
        try {
            Statement stmt = service.getService();
            String sql = "SELECT * FROM COURSE";
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                String courseName = rs.getString("courseName");
                int room = rs.getInt("room");
                String habitat = rs.getString("habitat");
                allCourses.add("Coursename: " + courseName + ", in room: " + room + ", @Location: " + habitat);
            }
            System.out.println(allCourses);
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
            e.getCause();
        }
        System.out.println("Goodbye!");
        return allCourses;
    }
}