package com.trivadis.trainee.administration.sql;

import com.trivadis.trainee.administration.model.Address;
import com.trivadis.trainee.administration.model.Course;
import com.trivadis.trainee.administration.model.Habitat;
import com.trivadis.trainee.administration.model.Room;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class AllInsertMethods {

    public static List<Address> createTrivadisAddresses() {

        List<String> trivadisStreets = Arrays.asList(
                "Elisabethenanlage",
                "Weltpoststrasse",
                "Badenerstrasse",
                "George Constantinescu Street Globalworth Campus, B building, 1st floor",
                "Werdener Strasse",
                "Atricom, Lyoner Strasse",
                "Heinrich-von-Stephan-Str.",
                "Route de Peney, Postfach 678",
                "Paul-Dessau-Strasse , Gebäude P",
                "Rue Marterey",
                "Weinheimer Strasse",
                "Riem Arcaden/Neue Messe, Lehrer-Wirth-Strasse",
                "Industriestrasse",
                "Gertrude-Fröhlich-Sandner-Straße, QBC 1, Top 7, 2. OG",
                "Sägereistrasse");
        List<String> trivadisZIP = Arrays.asList("4051", "3015", "5200", "020339", "40227","60528", "79100", "1214", "22761", "1005", "68309","81829", "70565", "1100","8152");
        List<String> trivadisHouseNumber = Arrays.asList("9", "5", "13", "2-4B", "4", "15", "17", "2", "6", "5", "68", "4", "4", "1", "29");
        List<String> trivadisLocation = Arrays.asList("BASEL", "BERN", "BRUGG", "BUKAREST", "DUESSELDORF", "FRANKFURT_AM_MAIN",
                "FREIBURG", "GENF", "HAMBURG", "LAUSANNE", "MANNHEIM", "MUENCHEN", "STUTTGART", "WIEN", "ZUERICH");

        List<Address> addresses = new ArrayList<>();
        for (int i = 0; i < trivadisStreets.size(); i++) {

            addresses.add(new Address(trivadisStreets.get(i), trivadisZIP.get(i), trivadisHouseNumber.get(i), Habitat.valueOf(trivadisLocation.get(i))));

        }

        return addresses;
    }

    private static Habitat randomHabitat() {
        int pick = new Random().nextInt(Habitat.values().length);
        return Habitat.values()[pick];
    }

    public static List<Room> createRandomRooms(int numbers) {

        Random r = new Random();
        int low = 50;
        int high = 200;

        List<Room> rooms = new ArrayList<>();
        for (int i = 0; i < numbers; i++) {
            int randomMax = r.nextInt(high-low) + low;
            Habitat h = randomHabitat();
            rooms.add(new Room(i, h, randomMax));
        }

        return rooms;

    }

    private static Course.CourseName randomCourse() {
        int pick = new Random().nextInt(Course.CourseName.values().length);
        return Course.CourseName.values()[pick];
    }

    public static List<Course> createTrivadisCourses(int numbers) {
        List<Course> courses = new ArrayList<>();
        for (int i = 0; i < numbers; i++) {
            Course.CourseName c = randomCourse();
            Habitat h = randomHabitat();
            courses.add(new Course(i, c, h));
        }

        return courses;

    }
}

