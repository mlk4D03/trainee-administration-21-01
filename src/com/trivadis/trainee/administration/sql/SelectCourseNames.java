package com.trivadis.trainee.administration.sql;

import com.trivadis.trainee.administration.service.Service;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class SelectCourseNames {

    Service service = new Service();

    public List<String> selectCourseNames() {
        List<String> courseNames = new ArrayList<>();
        try{
            Statement stmt = service.getService();
            String sql = "SELECT courseName FROM COURSE";
            ResultSet rs = stmt.executeQuery(sql);
            while(rs.next()){
                String courseName = rs.getString("courseName");
                courseNames.add(courseName);
            }
            courseNames = courseNames.stream()
                    .distinct()
                    .collect(Collectors.toList());
            System.out.println(courseNames);
            rs.close();

        }catch(SQLException e){
            e.printStackTrace();
        }catch(Exception e){
            e.printStackTrace();
            e.getCause();
        }
        System.out.println("Goodbye!");
        return courseNames;
    }

}