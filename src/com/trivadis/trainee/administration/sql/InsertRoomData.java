package com.trivadis.trainee.administration.sql;

import com.trivadis.trainee.administration.model.Room;
import com.trivadis.trainee.administration.service.Service;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public class InsertRoomData {

    Service service = new Service();

    public void insertRoomData(){
        try{
            Statement stmt = service.getService();
            int quantity = 10;
            List<Room> rooms = AllInsertMethods.createRandomRooms(quantity);
            for (Room r : rooms) {
                String query = String.format("INSERT INTO ROOM VALUES (%d, %d, '%s')", r.getNumber(), r.getMaxPerson(), r.getCity());
                stmt.executeUpdate(query);
            }
            System.out.println("Room records inserted successfully...");
        }catch(SQLException e){
            e.printStackTrace();
        }catch(Exception e){
            e.printStackTrace();
            e.getCause();
        }
        System.out.println("Goodbye!");
    }
}