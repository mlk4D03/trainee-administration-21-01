package com.trivadis.trainee.administration.sql;

import com.trivadis.trainee.administration.service.Service;
import java.sql.*;

public class CreateDB {
    Service service = new Service();

    public void createDB() {
        try{
            Statement stmt = service.getService();
            String dbName = "trainee_administration";
            String sql = "CREATE DATABASE " + dbName;
            stmt.executeUpdate(sql);
            System.out.println("DB created successfully...");
        }catch(SQLException e){
            e.printStackTrace();
        }catch(Exception e){
            e.printStackTrace();
            e.getCause();
        }
        System.out.println("Goodbye!");
    }
}