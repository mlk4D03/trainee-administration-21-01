CREATE TABLE HABITAT (
    habitat VARCHAR(255) NOT NULL,
    street VARCHAR(255) NOT NULL,
    number VARCHAR(255) NOT NULL,
    zip VARCHAR(255) NOT NULL,
    PRIMARY KEY ( habitat ));

CREATE TABLE ROOM (
    number INTEGER NOT NULL,
    habitat VARCHAR(255) NOT NULL,
    maxPerson INTEGER NOT NULL,
    PRIMARY KEY ( number ),
    FOREIGN KEY ( habitat ) REFERENCES HABITAT ( habitat ));

CREATE TABLE COURSE (
    courseName VARCHAR(255) NOT NULL,
    room Integer NOT NULL,
    habitat VARCHAR(255) NOT NULL,
    PRIMARY KEY ( courseName, room, habitat ),
    FOREIGN KEY ( room ) REFERENCES ROOM ( number ));

CREATE TABLE TRAINEE (
    traineeId INTEGER NOT NULL AUTO_INCREMENT,
    lastName VARCHAR(255) NOT NULL ,
    firstName VARCHAR(255) NOT NULL,
    habitat VARCHAR(255) NOT NULL,
    courseName VARCHAR(255) NOT NULL,
	ageGroup varchar(255) NOT NULL,
    previousKnowledge INTEGER NOT NULL,
	PRIMARY KEY ( traineeId ),
    FOREIGN KEY ( habitat ) REFERENCES HABITAT ( habitat ),
    FOREIGN KEY ( courseName ) REFERENCES COURSE ( courseName ));