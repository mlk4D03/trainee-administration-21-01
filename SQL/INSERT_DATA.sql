INSERT INTO HABITAT (habitat, street, number, zip)
VALUES
('BASEL', 'Elisabethenanlage', '9', '4051'),
('BERN', 'Weltpoststraße', '5', '3015'),
('BRUGG', 'Badenerstrasse', '13', '5200'),
('BUKAREST', 'George Constantisescu Street', '2-4B', '020339'),
("DUESSELDORF", "Werdener Straße", "4", "40227"),
("FRANKFURT AM MAIN", "Lyoner Strasse" ,"15", "60528"),
("FREIBURG I.BR", "Heinrich-von-Stephan-Strasse", "17", "79100"),
("GENF", "Route de Peney" , "2", "1214"),
("HAMBURG", "Paul-Dessau-Strasse" , "6", "22761"),
("LAUSANNE", "Rue Marterey", "5", "1005"),
("MANNHEIM", "Weinheimer Strasse", "68", "68309"),
("MUENCHEN", "Lehrer-Wirth-Strasse", "4", "81829"),
("STUTTGART", "Industriestrasse", "4", "70565"),
("WIEN", "Gertrude-Fröhlich-Sandner-Straße", "1", "1100"),
("ZUERICH", "Sägereistrasse", "29", "8152");

INSERT INTO ROOM (number, maxPerson, habitat)
VALUES 
(1, 10, 'BERN'),
(2, 100, 'DUESSELDORF'),
(3, 150, 'STUTTGART');

INSERT INTO COURSE
VALUES
('AZURE', 1),
('BI', 2),
('JAVA', 3);

INSERT INTO TRAINEE (lastName, firstName, previousKnowledge, ageGroup, habitat, courseName) 
VALUES 
('Grioui', 'Amine', 7, YEAR211, 'STUTTGART', 'Java'),
('Koch', 'Anette', 7, 2001, 'MANNHEIM', 'Java'),
('Friedrich', 'Jimmy', 7, 2002, 'BERN', 'Java'),
('Döffinger', 'Mika', 7, 2003, 'HAMBURG', 'Java'),
('Heer', 'Phillip', 7, 2004, 'DUESSELDORF', 'Java');


